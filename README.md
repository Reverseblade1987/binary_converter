# binary converter

10進数から2進数へ変換するプログラムです。

  - 10進数を入力します
  - 計算式を表示するか選びます
  - 変換結果が表示されます


Yuta Fujisawa  
<https://bitbucket.org/Reverseblade1987>
