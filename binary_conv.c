#include <stdio.h>
#include <string.h>

int main(){

   int a;
   int b;
   int c[255];
   char opt[100];
   
   printf("2進数に変換します。\n10進数の値を入力してください > ");
   scanf("%d", &a);
   printf("計算式を表示しますか? y(yes) or n(no) > ");
   scanf("%s", opt);
   
   if(strcmp(opt, "y") == 0){
        printf("\n計算式: \n"); 
   }
   
   int i = 0;
   while(a > 0){
       b = a / 2;
       c[i] = a % 2;
       
       if(strcmp(opt, "y") == 0){
           printf("%d ÷ 2 = %d 余り %d\n", a, b, c[i]);
       }
       
       a = b; 
       i++;
   }
   
   printf("\n変換結果： ");
   
   int j;
   for(j = i-1; j >= 0; j--){
      printf("%d", c[j]);
   }
   
   printf("\n");
   return 0;
}
